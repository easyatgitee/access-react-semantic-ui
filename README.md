   if (role !== "a2j") {
    window.location.href = `${window.location.origin}/advocate`;
  }

    <PrivateRoute
          exact
          path="/advocate"
          component={() => <AdvocateDBoard role={role} setRole={setRole} />}
        />
        <PrivateRoute
          exact
          path="/a2j"
          component={() => <A2JDBoard role={role} setRole={setRole} />}
        />
        <Route path="/" component={LoginPage} />
      </Switch>