import React from 'react';
import { Dropdown,Modal,Button,Segment,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";
import up from '../assets/up.png';
import down from '../assets/down.png';
export const TitleArea = (props) => { 
  return ( <div style={{  display: "flex", justifyContent: "space-between", }}> 
    <div style={{color:'#42526E',padding: '0px 0px 20px 0px',fontSize:24,fontWeight:700}}>{ props.title }</div>
      { props.back }
  </div> );
};
export const Between = (props) => { 
  return ( <div style={{ 
    width:'100%',
    display:'flex',justifyContent:'space-between'
   }}> { props.content }</div> );
};
export const End = (props) => { 
  return ( <div style={{ 
    width:'100%',
    display:'flex',justifyContent:'flex-end'
   }}> { props.content }</div> );
};
export const Empty = (props) => { 
  return ( <div style={{ 
    width:'100%', 
   }}> 
   <div style={{ 
    width:'100%',
    display:'flex',justifyContent:'center'
   }}> { props.img }</div>
   <div style={{ 
    width:'100%',
    display:'flex',justifyContent:'center'
   }}> { props.msg }</div>

  </div> );
};
export const Badge = (props) => { 
  return ( <div style={{ 
        color: "#FFF", 
        backgroundColor: "red",
        borderWidth: 0,
        borderRadius: 50,
        width:20,
        height:15,
        display: 'inline-flex', 
        visibility: props.number == 0  ? "hidden" : "inherit",
        justifyContent:'center',
        alignItemst:'center',
        fontWeight: 800
   }}> { props.number }</div> );
};
 
export const getBigCard = (title,val,reate,boxShadow = false) => {
  
  return  <Card style={{ 
     borderRadius:8, 
     boxShadow:boxShadow?'0 0 0 0 ':'0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5'
    }} >
    <Card.Content style={{ padding:boxShadow?0:'1em'}}>
      <h3 style={ {    color:"#42526E"  }}> { title } </h3> 
    <div  style={ {   display:'flex',justifyContent:'space-between' }}>
      <span style={ {    color:"#42526E",fontSize:34,fontWeight:800   }} > { val }</span>  
      <span style={ {     color:'#5480C2',fontSize:18 ,fontWeight:600   }} > { reate }</span>
    </div> 
  </Card.Content>
</Card>
}

export const getCard = (title,val,reate) => { 
  var src = reate == 1? up:down
  return  <Card style={{   borderRadius:8, }} > 
    <Card.Content style={{   padding:'8px 10px', }}> 
      <div  style={ {   display:'flex',justifyContent:'space-between' }}>
       <span style={ {   width:'50%',  color:"#42526E"  }}> { title } </span> 
       <span style={ {    color:'#5480C2',fontSize:18,fontWeight:600    }} > { val }</span>  
       <img src={src} />
    </div> 
  </Card.Content>
</Card>
}