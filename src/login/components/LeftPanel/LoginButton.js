import React from "react";

import { useAuth0 } from "@auth0/auth0-react";

const LoginButton = () => {
  const { loginWithRedirect } = useAuth0();

  return (
    <button
      onClick={() => loginWithRedirect()}
      id="qsLoginBtn"
      variant="primary"
      className="circular ui button"
      type="submit"
      style={{
        backgroundColor: "#00282A",
        color: "white",
        width: "10vw",
        marginTop: "6vh",
        marginLeft: "9vh",
      }}
    >
      Login
    </button>
  );
};

export default LoginButton;
