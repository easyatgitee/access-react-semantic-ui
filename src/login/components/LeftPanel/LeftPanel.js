import React from "react";
import hello from "./hello.png";
import LoginForm from "./LoginForm";

const textStyle = {
  position: "relative",
  fontSize: "2vh",
  color: "#748281",
  top: "20vh",
  left: "4.5vw",
};

const helloStyle = {
  position: "relative",
  width: "20vw",
  top: "23vh",
  left: "4vw",
};

const LeftPanel = () => {
  return (
    <div>
      <a
        href="https://www.google.ca/"
        className="vertically fitted item"
        style={{
          position: "relative",
          top: "5vh",
          left: "29vw",
          color: "#00282A",
        }}
      >
        <i className="big question circle icon"></i>
      </a>
      <a
        href="https://www.google.ca/"
        className="vertically fitted item"
        style={{
          position: "relative",
          top: "5vh",
          left: "30vw",
          color: "#00282A",
        }}
      >
        <i className="big chevron circle right icon"></i>
      </a>
      <div className="Login-text" style={textStyle}>
        LOGIN
      </div>
      <img className="Hello" src={hello} alt="" style={helloStyle} />
      <LoginForm />
    </div>
  );
};

export default LeftPanel;
