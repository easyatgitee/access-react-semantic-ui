import React from "react";
import background from "./bg2.png";
import Welcome from "./Welcome.png"; 
import LeftPanel from "./components/LeftPanel/LeftPanel";
import RightPanel from "./components/RightPanel/RightPanel";
import { Header,Dropdown,Modal,Button,Segment,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";

const backgroundStyle = {
  display: "inline",
  position: "absolute",
  width: "100%",
  height: "100%",
  backgroundImage: `url(${background})`,
  backgroundPosition: "center",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
};
const Btn1 ={  
  backgroundColor: "#C2C3BE",
  color: "white",
  width: "10vw",
  borderRadius: 50,
  border: "1px solid #FFF ",
  marginTop: 30,
 } 
 const Btn2 ={  
  backgroundColor: "#00282A",
  color: "white",
  width: "10vw",
  borderRadius: 50,
  marginTop: 30,
 } 
const textStyle = { fontSize: "30px", color: "#00282A", fontWeight: 800, margin:'20px 120px',display: "flex",justifyContent: "center", };
const flex ={ display: "flex",justifyContent: "center", }  
const Wapper = {   marginTop: 0,backgroundColor: "#fff", borderColor: "#000", border: "1px solid #CCC", borderRadius: 50, padding:'2px 10px',   }
const options = [
  { key: 'angular', text: 'Angular', value: 'angular' },
  { key: 'css', text: 'CSS', value: 'css' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' }, 
]

const LoginPage = () => {
  return (
    <div style={backgroundStyle}>
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column style={{ width: "35vw" }}>
            {/* 左侧gard 5行* */}
             <Grid columns={2}>
               
               <Grid.Row >  
                 <Button style={{ margin:'20px 40px',borderRadius: 50,backgroundColor: "#C2C3BE",color: "#fff", border: "2px solid #fff",}}>
                  <Icon name='arrow left' /> Back
                </Button>
                </Grid.Row>  
                <Grid.Row  >  
                  <div style={ { fontSize: "50px", color: "#fff", fontWeight: 800, margin:'120px 120px 0 120px',  } }>
                    Already have an </div>
                </Grid.Row>  
                <Grid.Row  >  
                  <div style={ { fontSize: "50px", color: "#fff", fontWeight: 800, margin:'20px 120px',  } }>
                     account？</div>
                </Grid.Row>  
                <Grid.Row  >  
                  <div style={ { fontSize: "20px", color: "#8A928F", fontWeight: 400, margin:'20px 140px',  } }>
                    Access the login page here</div>
                </Grid.Row>  
               <Grid.Row style={flex} >  
                 <Button style={Btn1} onClick={ ()=>{   }} >Login Page</Button>
               </Grid.Row>  

              </Grid>
           
             {/**/}
          </Grid.Column>
          <Grid.Column>
            {/* 右侧gard 5行* */}
            <Grid >  
               
                <Grid.Row style={flex} >  
                   <img   style={{ marginTop:100}}  src={Welcome} />>
                </Grid.Row>   

                <Grid.Row style={flex} >  
                  <div style={Wapper}>
                    <Input  placeholder="First name" >  <input style={{ border: 0,width:"9vw",  }} />  </Input>
                 </div>
                 <div style={Wapper}>
                  <Input  placeholder="Last name" >  <input style={{ border: 0,width:"9vw",  }} />  </Input>
               </div>
                </Grid.Row>  

                <Grid.Row style={flex} >  
                  <div style={Wapper}>
                    <Dropdown  placeholder='City '  fluid   selection  options={options}  style={{ border: 0,width:"21vw",  }}/>
                 </div>
                 
                </Grid.Row>   
                <Grid.Row style={flex} >  
                   
                 <div style={Wapper}>
                  <Dropdown  placeholder='Organization '  fluid   selection  options={options}  style={{ border: 0,width:"21vw",  }}/>
                </div>

                </Grid.Row> 

                <Grid.Row style={flex} >  
                  <div style={Wapper}>
                    <Input icon iconPosition='left'   placeholder="Email" >  <Icon name="mail outline" /> <input style={{ border: 0,width:"21vw",  }} />  </Input>
                 </div>
                </Grid.Row>   
                <Grid.Row style={flex} >  
                  <div style={Wapper}>
                    <Input icon iconPosition='left'   placeholder="Password" >  <Icon name="lock" /> <input type="password" style={{ border: 0,width:"20vw",  }} />  </Input><Icon name="eye" /> 
                   </div>
                </Grid.Row>   
                <Grid.Row style={flex} >  
                  <div style={Wapper}>
                    <Input icon     placeholder="Re-Enter Password" >  <Icon name="eye" /> <input type="password" style={{ border: 0,width:"20vw",  }} />  </Input> <Icon name="eye" /> 
                   </div>
                </Grid.Row>  

                <Grid.Row  style={{ color:'#7F8D8A',margin:'0 250px' }}>  
                  <Checkbox />   <div style={{ color:'#7F8D8A',margin:'0 10px' }}>Accept teams and prcance</div>
                </Grid.Row> 
              
      
                 <Grid.Row >  
                  <Grid.Column width={12}>  <i  style={{ width: "3vw",fontSize:30, margin:'50px 0 0 100px' }} className="big question circle icon"></i> </Grid.Column>
                  <Grid.Column>  <Button style={Btn2} onClick={ ()=>{   }} > Create Account </Button></Grid.Column>
                  
                  
                 </Grid.Row>  
                </Grid>
              
            {/**/}
             
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default LoginPage;
