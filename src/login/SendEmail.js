import React, { useState, useEffect} from 'react';
import background from "./background.png";
import { Header,Dropdown,Modal,Button,Segment,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";
import LeftPanel from "./components/LeftPanel/LeftPanel";
import RightPanel from "./components/RightPanel/RightPanel";

const backgroundStyle = {
  paddingTop:30,
  display: "inline",
  position: "absolute",
  width: "100%",
  height: "100%",
  backgroundImage: `url(${background})`,
  backgroundPosition: "center",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
};

const textStyle = { fontSize: "2vh", color: "#748281", margin:'10px 100px' };
const middleTxt1 = { fontSize: "30px", color: "#00282A", fontWeight: 400, margin:'10px 110px' };
const middleTxt2 = { fontSize: "30px", color: "#00282A", fontWeight: 400, margin:'20px 100px' };
const middleTxt3 = { fontSize: "30px", color: "#00282A", fontWeight: 400, margin:'10px 120px' };
const middleTxt4 = { fontSize: "30px", color: "#00282A", fontWeight: 400, margin:'10px ' };
const textStyle2 = { fontSize: "2vh", color: "#748281", margin:'110px 1px 0 0',display: "flex",justifyContent: "center", };
const bgTxt = { fontSize: "30px", color: "#00282A", fontWeight: 800, margin:'20px 120px',display: "flex",justifyContent: "center", };
const flex ={ display: "flex",justifyContent: "center", } 
const flex1 ={ display: "flex",justifyContent: "center", marginTop: 80,} 
const emailWapper = {   marginTop: 80,backgroundColor: "#fff", borderColor: "#000", border: "1px solid #CCC", borderRadius: 50, padding:'2px 10px',  margin:5, }
const sendBtn ={  
  backgroundColor: "#00282A",
  color: "white",
  width: "10vw",
  borderRadius: 50,
  marginTop: 30,
 } 
const LoginPage = () => {
  const [init, setInit] = useState(true);
const [email, setemail] = useState("");

const send =()=>{
  // if(init && !/\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/.test(email))return message.error("Pleass enter right email");
   
  setInit(!init);

}

  return (
    <div style={backgroundStyle}> 
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column style={{ width: "35vw" }}> 

         <Grid > 
        <Grid.Row>
          <Grid.Column style={{ width: "3vw" }}> </Grid.Column>
          <Grid.Column width={9}>
            <Button style={{ borderRadius: 50,backgroundColor: "#F7EDE4",color: "#A0A9A6", border: "2px solid #A0A9A6",}}>
              <Icon name='arrow left' /> Back
            </Button>
          </Grid.Column>
          <Grid.Column style={{ width: "3vw",fontSize:20,  }}>   <i className="big question circle icon"></i> </Grid.Column>
          <Grid.Column style={{ width: "3vw" }}> </Grid.Column>
          <Grid.Column style={{ width: "3vw",fontSize:20,  }}>   <i className="big chevron circle right icon"></i></Grid.Column>
           
           </Grid.Row>
		       <Grid.Row style={textStyle}> Forget Password  </Grid.Row> 



          <div style={{ display:!init?'none':"", }}>  
            <Grid.Row style={middleTxt1}>  Enter your account's email </Grid.Row>
            <Grid.Row style={middleTxt2}>and we'll send you an email </Grid.Row>
            <Grid.Row style={middleTxt3}>  to reset the password... </Grid.Row> 
            <Grid.Row style={ flex1 }>  
              <div style={emailWapper}>
                 <Input icon iconPosition='left'   placeholder="Email" >  <Icon name="mail outline" /> <input style={{ border: 0,width:"20vw",  }} />  </Input>
              </div>
             </Grid.Row>  
          </div>

           <div style={{ display:init?'none':"",width:'100%' }}>
            <Grid.Row style={bgTxt} > Please Check </Grid.Row>
            <Grid.Row style={bgTxt}> Your email. </Grid.Row> 
            <Grid.Row style={middleTxt4} > We have sent a password reset instruction to </Grid.Row>
            <Grid.Row style={middleTxt1}  > your email. </Grid.Row> 
            <Grid.Row style={textStyle2} > I didn't receive the password reset letter... </Grid.Row>
          </div>

          

           <Grid.Row style={flex} >  
             <Button style={sendBtn} onClick={ ()=>{  send()  }} > { init?'Send Email':"Resend Email" } </Button>
           </Grid.Row>  
          </Grid>
            
          </Grid.Column>
          <Grid.Column>
             
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default LoginPage;
