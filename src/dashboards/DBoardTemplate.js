import React from "react";
import LogoutButton from "./LogoutButton";

const DBoardTemplate = (props) => {
  const { change } = props 
  return (
    <div>
      <div className="ui borderless massive menu">
        <div className="header item">
          <img
          className="ui image"
          src={process.env.PUBLIC_URL + "/app-switcher.png"}
          alt="team logo"
          style={{ width: "30px" }}
          onClick={()=>{
            change()
          }}
        />
          <img
            className="ui image"
            src={process.env.PUBLIC_URL + "/extracy.png"}
            alt="team logo"
            style={{ width: "100px" }}
          />
        </div>
        <div className="right item">
          <a href="https://www.google.ca/" className="vertically fitted item">
            <i className="question circle icon"></i>
          </a>
          <LogoutButton />
        </div>
      </div>

      {props.content}
    </div>
  );
};

export default DBoardTemplate;
