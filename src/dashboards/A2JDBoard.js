import React, { useState } from "react";
import DBoardTemplate from "./DBoardTemplate";
import { Grid, Menu, Segment, Icon } from "semantic-ui-react";
import { useAuth0 } from "@auth0/auth0-react";
import { TitleArea ,Between,End,Empty,Badge} from '../pageCore/PageCore';

// 高权限能看到的页面






// 低权限能看的的页面 
import Overview from '../views/OverView';
import ViewData from '../views/ViewData';

import AdvocateList from '../views/AdvocateList';
import Visualiztion from '../views/Visualiztion';

// 共同看的的页面
import Overviews from '../views/OverViews';
import Submission from '../views/Submission';

import Setting from '../views/Setting';

const A2JDBoard = (props) => {
  const { role } = props;
  if (role !== "a2j") {
    window.location.href = `${window.location.origin}/advocate`;
  }
  const { user } = useAuth0();

  const [activeItem, setActiveItem] = useState("overviews");

  const [Collapsed, setCollapsed] = useState(false);

  const [type, setType] = useState(0);

  const handleItemClick = (e, { name }) => {
    setActiveItem(name);
  };

  const agency = user.email;

  // import content logic in here

  const setMenu =()=>{

  }
  const renderSwitch = (state) => {
    switch (state) {
      case "overview":  return <Overview setMenu = { setMenu }/>;
      case "overviews":  return <Overviews setActiveItem/>;
      case "new": return <Submission/>;
      case "Visualiztion": return <Visualiztion />;
      case "AdvocateList":  return <AdvocateList/>; 
      case "settings":  return  <Setting/>; 
      case "ViewData":  return <ViewData setActiveItem/>;
      default:  return "Error Occured, contact system admin";
    }
  };
  
  const getIcon =(name)=>{
     return {color: activeItem === name ?'#0052CC':'#42526E'};
  }
  const change = ()=>{ 
    console.log(Collapsed)
    setCollapsed(!Collapsed)
  }

  const content = (
    <Grid style={{ height: "100vh" }}>
      <Grid.Column width = { Collapsed ? 1: 2  } style={{ width:100, backgroundColor: "rgb(250, 251, 252)" }}> 
        <div className="user item flex"  style={{ padding: '0 10px' }}>
          <img
          className="ui image"
          src={process.env.PUBLIC_URL + "/user.png"}
          alt="team logo"
          style={{ width: "50px" }}
        />
         <div  style={{ padding: '0 10px', 
         display: Collapsed ? "none" : "block",
        }}>
          <h3>Advocate</h3> 
          {agency}
         </div>
        </div>
        <Menu  secondary vertical color="blue" >  
          <Menu.Item name="overview" active={activeItem === "overview"} onClick={handleItemClick} >
               <Icon name="clone"  style={  getIcon("activeItem")}/> <div className="menuLabel" style={{   display: Collapsed ? "none" : "block", }} >Overview </div> <Badge number={ Collapsed?0:1}/>
          </Menu.Item>
          <Menu.Item name="ViewData" active={activeItem === "ViewData"} onClick={handleItemClick} >
            <Icon name="table"  style={  getIcon("ViewData")}/> <div className="menuLabel" style={{   display: Collapsed ? "none" : "block", }} >View Data </div> <Badge number={ Collapsed?0:0}/>
          </Menu.Item>
          <Menu.Item name="Visualiztion" active={activeItem === "Visualiztion"} onClick={handleItemClick} >
            <Icon name="line graph" style={  getIcon("Visualiztion") }/> <div className="menuLabel" style={{   display: Collapsed ? "none" : "block", }}> Visualization </div> <Badge number={Collapsed?0:0}/>
          </Menu.Item>
          <Menu.Item name="AdvocateList" active={activeItem === "AdvocateList"} onClick={handleItemClick} >
            <Icon name="users" style={  getIcon("AdvocateList") }/> <div className="menuLabel" style={{   display: Collapsed ? "none" : "block", }}>Advocate List </div> <Badge number={Collapsed?0:0}/>
          </Menu.Item>
          <Menu.Item name="settings" active={activeItem === "settings"} onClick={handleItemClick} >
            <Icon name="cog" style={ getIcon("settings") }/> <div className="menuLabel" style={{   display: Collapsed ? "none" : "block", }}>Setting </div> <Badge number={Collapsed?0:0}/>
          </Menu.Item> 
        </Menu>
      </Grid.Column>

      <Grid.Column stretched width={14}>
        <Segment style={{ padding:40 }}>{renderSwitch(activeItem)} </Segment>
      </Grid.Column>
    </Grid>
  );

  return (
    <div>
      <DBoardTemplate content={content}   change = { change }/>
    </div>
  );
};

export default A2JDBoard;
