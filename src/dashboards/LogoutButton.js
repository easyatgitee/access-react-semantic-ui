import React, { useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { Modal,Button } from "semantic-ui-react";
const LogoutButton = () => {
  const { logout } = useAuth0();
  const [open, setOpen] = useState(false);
  return (
    <div>
      <Button  size="mini" onClick={()=>{ setOpen(true) } }    style={{ backgroundColor: "#F5F6F8", color: "#8993A4", }} >
      Log out
    </Button>
    
    <Modal open={open} size="small" style={ {  width: 400 }}
      content={
        <div style={ {  padding:20, }} >
        <p style={ {  color:"#42526E",fontSize:18,fontWeight:600  }}>  Are you sure want to log out ?</p>
        <div style={{ display:'flex' ,  justifyContent: "flex-end",}}>
          <Button    onClick={() => { setOpen(false); }} size = "mini" style={{ backgroundColor: "#fff", color: "#8993A4", borderRadius:2,}}>Cancel</Button>
          <Button    onClick={() => { setOpen(false); logout(); localStorage.setItem("role", null); }}   size = "mini" style={{ backgroundColor: "#0052CC", color: "#fff", borderRadius: 2,}}>Log Out</Button>
        </div> 
        </div>
      }
    />
    </div>
  );
};

export default LogoutButton;
