import React, { useState } from "react"; 
import { Progress,Header,Dropdown,Modal,Button,Segment,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";
import { TitleArea ,Between,End,Empty,Badge,getBigCard,getCard} from '../pageCore/PageCore';

import Pine from '../pageCore/pine';
import Line from '../pageCore/line';
import Bar from '../pageCore/bar';
import BarBG from '../pageCore/barBg';

import Switch from '../components/Switch'
const style = {
  // 布局
  display: "flex", justifyContent: "space-between",
  flexDirection: "column",
  alignItems: "center",
  height: 11,
  width: 11,
  
  textAlign: "center",  
  // 控制
  visibility: 1 ? "hidden" : "inherit",
  display: 1 ? "none" : "block",
  // 文字
  color: "#FFF",fontSize: 34,  fontWeight: 800,
  //  背景边框
  backgroundColor: "#000", border: '1px solid #CCC',borderRadius: 8,
  
  borderColor: "#000",
  borderWidth: 3,
  
  margin: "0 3px",
  padding: "30px",
  
};
export default  () => { 
  const [open, setOpen] = useState(false);

  const [initProfile, setProfileCard] = useState(false);   
  const [initNotice, setNoticeCard] = useState(false);   
  const [initEmail, setEmailCard] = useState(false);   
  const [initPassword, setPasswordCard] = useState(false);   
  const [showModal, setShowModal] = useState(false);   
  const countryOptions = [
  { key: 'af', value: 'af', text: 'Afghanistan' },
  { key: 'ax', value: 'ax', text: 'Aland Islands' }, 
]

const options = [
  { key: 'June', text: 'June', value: 'June' },
  { key: 'July', text: 'July', value: 'July' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' }, 
]
const years = [
  { key: '2021', text: '2021', value: '2021' },
  { key: '2022', text: 'CSS', value: 'css' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' }, 
]
const flex = {
  display:'flex',
}
console.log(TitleArea)
  return (
    <div style={{ padding:100}} >  
      <TitleArea title="Test"/> 
      <Switch checked={false}/>
      <span style={{   backgroundColor: '#FFF0B3', color: '#172B4D', padding: '3px 10px',  borderRadius: 4, }}>NEW</span>

      <span style={{   backgroundColor: '#E3FCEF', color: '#006644', padding: '3px 10px',  borderRadius: 4, }}>VIEWED</span>


      <Card style={{ width:'25vw'}}> 
        <Card.Content>
          <div>
            <div>
              <Dropdown compact   defaultValue="June"  selection  options={options}  
              style={{ border: 0, color:'#C4C4C4' , backgroundColor: "#F4F5F7", marginRight:10, width:100  }}/>
              <Dropdown compact    defaultValue="2021"  selection  options={years}  
              style={{ border: 0, color:'#C4C4C4' , backgroundColor: "#F4F5F7",}}/>
             <span style={{ position: 'absolute', right: 20, top:20, backgroundColor: '#B3D4FF', color: '#0747A6', padding: '3px 10px',  borderRadius: 4, }}>75%</span>
            </div>
            <div style={{ fontSize:20, fontWeight: 900,color:'#42526E', height:70,paddingTop:10}}>
              Data Submission
            </div>
            <div style={{ display:'flex',justifyContent: "space-between", height:70}}>
              <span style={{ fontSize:80, fontWeight: 900,color:'#42526E'}} >90</span>
              <span style={{ fontSize:30, fontWeight: 900,color:'#0747A6'}}>+23</span>
            </div> 
            <div style={{  height:20 }}>
              <Progress size="small" color="blue" percent={75} />
            </div>
            <div style={{ display: "flex",justifyContent: "space-between", }}>
              <span style={{ display:'inline-block',width:'75%',color:'#0747A6',textAlign:'end'}} >90</span>
              <span style={{ color:'#C4C4C4'}}>120</span>
            </div>
            <div style={{ display: "flex",justifyContent: "flex-end", }}>
              <Button style={{ backgroundColor: "#0747A6", color: "#fff",}}>View Data</Button> 
              <Button style={{ backgroundColor: "#F5F6F8", color: "#8993A4",}}>Advocate List</Button>
            </div>
          </div>


        </Card.Content> 
      </Card>
  

      <div style={{ 
        backgroundColor: "#fff",
        borderColor: "#000",
        border: "1px solid #CCC",
        borderRadius: 50,
        padding:'2px 10px',
        margin:5,
    }}>

    <Input icon iconPosition='left'   placeholder="Email" style={{  width:'98%' }}>
        <Icon name="mail outline" />
        <input style={{ border: 0 }} /> 
      </Input>
      </div>

      <div style={{ 
        backgroundColor: "#fff",
        borderColor: "#000",
        border: "1px solid #CCC",
        borderRadius: 50,
        padding:'2px 10px',
        margin:5,
    }}>

    <Input icon iconPosition='left'   placeholder="Password" style={{  width:'98%' }}>
        <Icon name="lock" />
        <input style={{ border: 0 }} /> 
      </Input>
      <Icon name="ellipsis horizontal" />
      </div>
     
       

     
      

 
       
        
 
 
      <Empty
      img={ <div>  
            <Icon name="check circle outline"  style={{ fontSize:50, }} />
      </div>}
      msg={ <div style={{ color:'#5E6C84',fontSize:24}}> 
        No New Sign-up Request </div>}
      />
      <Empty
      img={ <div>  
            <Icon name="smile outline"  style={{ fontSize:50, }} />
      </div>}
      msg={ <div style={{ color:'#5E6C84',fontSize:24}}> 
        Greate! You already submitted all data required!</div>}
      />
   
 
 
    </div>
  );
};

