import React, { useState } from "react";
import {Progress, Header,Dropdown,Modal,Button,Segment,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from
"semantic-ui-react";
import { TitleArea ,Between,End,Empty,Badge,getBigCard,getCard} from '../pageCore/PageCore';

import Pine from '../pageCore/pine';
import Line from '../pageCore/line';
import Line2 from '../pageCore/line2';
import Bar from '../pageCore/bar';
import BarBG from '../pageCore/barBg';

export default () => {
const [open, setOpen] = useState(false);

const [initProfile, setProfileCard] = useState(false);
const [initNotice, setNoticeCard] = useState(false);
const [initEmail, setEmailCard] = useState(false);
const [initPassword, setPasswordCard] = useState(false);
const [showModal, setShowModal] = useState(false);
const countryOptions = [
{ key: 'af', value: 'af', text: 'Afghanistan' },
{ key: 'ax', value: 'ax', text: 'Aland Islands' },
]

const options = [
{ key: 'June', text: 'June', value: 'June' },
{ key: 'July', text: 'July', value: 'July' },
{ key: 'design', text: 'Graphic Design', value: 'design' },
{ key: 'ember', text: 'Ember', value: 'ember' },
{ key: 'html', text: 'HTML', value: 'html' },
]
const years = [
{ key: '2021', text: '2021', value: '2021' },
{ key: '2022', text: 'CSS', value: 'css' },
{ key: 'design', text: 'Graphic Design', value: 'design' },
{ key: 'ember', text: 'Ember', value: 'ember' },
{ key: 'html', text: 'HTML', value: 'html' },
]
const flex = {
display:'flex',
}
console.log(TitleArea)
return (
<div>
  <TitleArea title="Overview" />


  <Grid>
    <Grid.Row>
      <Grid.Column width={6}>

        <Card style={{ width:'25vw',}}>
          <img src={process.env.PUBLIC_URL + "/bg1.png" } />
          <img src={process.env.PUBLIC_URL + "/icon1.png" } style={{ marginTop:-100, width:200, marginLeft:'30%' }} />
          <div style={{ display:'flex', justifyContent:'center', color:'#172B4D', height:30,paddingTop:10, fontSize: 24,
            fontWeight: 800 }}>You haven't submitted the data for</div>
          <div style={{ display:'flex', justifyContent:'center',height:70,paddingTop:10, color: '#0065FF' }}>June 2021，July 2021</div>
          <div style={{ display:'flex', justifyContent:'center',marginBottom:30  }}> 
            <Button style={{ backgroundColor: "#0747A6", color: "#fff",}}>Submit Now</Button> 
            <Button style={{ backgroundColor: "#fff", color: "#8993A4",}}>Remind me latter</Button>
          </div>
        </Card>

      </Grid.Column>

      <Grid.Column width={3}>
        { getBigCard("Total Case",189,"+16")}
        { getBigCard("Total Case",189,"+16")}
        { getBigCard("Total Case",189,"+16")}
      </Grid.Column>
      <Grid.Column width={3}>
        { getCard("Other",189,1)}
        { getCard("Other",189,0)}
        { getCard("Other",189,0)}
        { getCard("Other",189,1)}
        { getCard("Other",189,0)}
        { getCard("Other",189,0)}
      </Grid.Column>

    </Grid.Row>
    <Grid.Row>
      <Grid.Column width={8}>
        <Card style={{ width:'100%' }}>
          <Card.Content>
            <Line />
          </Card.Content>
        </Card>
      </Grid.Column>

      <Grid.Column width={8}>
        <Card style={{ width:'100%' }}>
          <Card.Content>
            <Line2 />
          </Card.Content>
        </Card>
      </Grid.Column>
    </Grid.Row>
  </Grid>




</div>
);
};