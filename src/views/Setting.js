import React, { useState } from "react"; 
import { Modal,Button,Segment,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";
import { TitleArea ,Between,End} from '../pageCore/PageCore';

import Switch from '../components/Switch'
export default  () => { 
  const [open, setOpen] = useState(false);

  const [initProfile, setProfileCard] = useState(false);   
  const [initNotice, setNoticeCard] = useState(false);   
  const [initEmail, setEmailCard] = useState(false);   
  const [initPassword, setPasswordCard] = useState(false);   
  const [showModal, setShowModal] = useState(false);   
  const countryOptions = [
  { key: 'af', value: 'af', text: 'Afghanistan' },
  { key: 'ax', value: 'ax', text: 'Aland Islands' }, 
]
console.log(TitleArea)
  return (
    <div > 
      <TitleArea title="Settings"/> 

     <Card style={{ width:'100%',color:'#5E6C84'}}>
        <Card.Content>
          <Grid onClick={()=>{ setProfileCard(true) }}>
            <Grid.Row>
                <Grid.Column  width={16} style={{ fontSize:30,  }}> <Icon name="user"  /> Profile Information </Grid.Column> 
            </Grid.Row> 
            <Grid.Row>
                <Grid.Column width={8} > First Name </Grid.Column>
                <Grid.Column width={8} > Last Name </Grid.Column>
              </Grid.Row>
            <Grid.Row>
              <Grid.Column width={8} > <Input fluid placeholder='Text'/>  </Grid.Column>
              <Grid.Column width={8} > <Input fluid placeholder='Text'/> </Grid.Column>
            </Grid.Row>
            <End  content = {
                <><Button    onClick={() => { setOpen(false); }} size = "mini" style={{ backgroundColor: "#fff", color: "#8993A4", borderRadius:2,}}>Cancel</Button>
                 <Button    onClick={() => { setOpen(false); }}   size = "mini" style={{ backgroundColor: "#0052CC", color: "#fff", borderRadius: 2,}}>Save</Button> </> } />
                 
            <Grid.Row>
                <Grid.Column width={12} >  </Grid.Column>
                <Grid.Column width={2} > 
                  
                </Grid.Column>
                <Grid.Column width={2} >  
                    
                </Grid.Column>
              </Grid.Row> 
            
      </Grid>
        </Card.Content>
      </Card> 

      <Card style={{ width:'100%',color:'#5E6C84'}}>
        <Card.Content>
          <Grid >
             <Grid.Row>
                <Grid.Column  width={15} style={{ fontSize:30,  }}>  <Icon name="bell outline"  />  Notification </Grid.Column>
                
             </Grid.Row> 
              <Grid.Row>
                <p style={{ margin:'0 10px'}}>Monthly data submission reminder </p>
                <Switch checked={true}/>

                <p style={{ margin:'0 10px 0 100px'}}>Remind me via email  </p> 
                <Switch checked={false}/>
              </Grid.Row> 
        </Grid> 
        </Card.Content>
      </Card> 




      <Card style={{ width:'100%',color:'#5E6C84'}}>
        <Card.Content>
          <Grid onClick={()=>{ setPasswordCard(true) }}>
                <Grid.Row>
                  <Grid.Column  width={15} style={{ fontSize:30,   }}>  <Icon name="lock"  />  Change Password </Grid.Column>
                  <Grid.Column width={1} style={{ fontSize:30,   }} > <Icon name="angle right" style={{ visibility:initPassword?'hidden':"inherit"  }} /></Grid.Column>
                </Grid.Row>
          </Grid> 
          <Grid style={{ display:!initPassword?'none':""  }} >
            <Grid.Row>
              <Grid.Column  width={15} style={{  }}>
                <Input fluid placeholder='Old Password'/>  
              </Grid.Column> 
            </Grid.Row>
            <Grid.Row>
              <Grid.Column  width={15} style={{  }}> 
                <Input fluid placeholder='Re Enter Password'/> 
              </Grid.Column> 
            </Grid.Row>
              <Grid.Row>
                <Grid.Column  width={15} style={{  }}>
                  <Button    onClick={() => {    }}   size = "mini" style={{ backgroundColor: "#0052CC", color: "#fff", borderRadius: 2,}}>Submit</Button> 
                </Grid.Column>
                <Grid.Column width={1} style={{ fontSize:30,   }} > <Icon name="angle up" onClick={()=>{ setPasswordCard(false) }} /></Grid.Column>
              </Grid.Row>
        </Grid> 
        </Card.Content>
      </Card> 
 


    <Modal open={open} size="small" style={ {  width: 400 }}
      content={
        <div style={ {  padding:20, }} >
        <p style={ {  color:"#42526E",fontSize:18,fontWeight:600  }}>  Are you sure want to log out ?</p>
        <End  content = {
            <><Button    onClick={() => { setOpen(false); }} size = "mini" style={{ backgroundColor: "#fff", color: "#8993A4", borderRadius:2,}}>Cancel</Button>
              <Button    onClick={() => { setOpen(false); }}   size = "mini" style={{ backgroundColor: "#0052CC", color: "#fff", borderRadius: 2,}}>Save</Button> </>
        } />


        </div>
      }
    />
    </div>
  );
};

