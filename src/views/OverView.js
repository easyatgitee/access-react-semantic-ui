import React, { useState } from "react"; 
import {Progress, Header,Dropdown,Modal,Button,Segment,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";
import { TitleArea ,Between,End,Empty,Badge,getBigCard,getCard} from '../pageCore/PageCore';

import Pine from '../pageCore/pine';
import Line from '../pageCore/line';
import Bar from '../pageCore/bar';
import BarBG from '../pageCore/barBg';

export default  (props) => { 
  const { setMenu } = props
  console.log(props)
  const [open, setOpen] = useState(false);

  const [initProfile, setProfileCard] = useState(false);   
  const [initNotice, setNoticeCard] = useState(false);   
  const [initEmail, setEmailCard] = useState(false);   
  const [initPassword, setPasswordCard] = useState(false);   
  const [showModal, setShowModal] = useState(false);   
  const countryOptions = [
  { key: 'af', value: 'af', text: 'Afghanistan' },
  { key: 'ax', value: 'ax', text: 'Aland Islands' }, 
]

const options = [
  { key: 'June', text: 'June', value: 'June' },
  { key: 'July', text: 'July', value: 'July' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' }, 
]
const years = [
  { key: '2021', text: '2021', value: '2021' },
  { key: '2022', text: 'CSS', value: 'css' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' }, 
]
const flex = {
  display:'flex',
} 

  return (
    <div > 
      <TitleArea title="Overview"/> 
       

    <Grid>
        <Grid.Row> 
            <Grid.Column width={6} >   
              <Card style={{ width:'25vw'}}> 
                <Card.Content>
                  <div>
                    <div>
                      <Dropdown compact   defaultValue="June"  selection  options={options}  
                      style={{ border: 0, color:'#C4C4C4' , backgroundColor: "#F4F5F7", marginRight:10, width:200  }}/>
                      <Dropdown compact    defaultValue="2021"  selection  options={years}  
                      style={{ border: 0, color:'#C4C4C4' , backgroundColor: "#F4F5F7",}}/>
                     <span style={{ position: 'absolute', right: 20, top:20, backgroundColor: '#B3D4FF', color: '#0747A6', padding: '3px 10px',  borderRadius: 4, }}>75%</span>
                    </div>
                    <div style={{ fontSize:20, fontWeight: 900,color:'#42526E', height:70,paddingTop:10}}>
                      Data Submission
                    </div>
                    <div style={{ display:'flex',justifyContent: "space-between", height:70}}>
                      <span style={{ fontSize:80, fontWeight: 900,color:'#42526E'}} >90</span>
                      <span style={{ fontSize:30, fontWeight: 900,color:'#0747A6'}}>+23</span>
                    </div> 
                    <div style={{  height:20 }}>
                      <Progress size="small" color="blue" percent={75} />
                    </div>
                    <div style={{ display: "flex",justifyContent: "space-between", }}>
                      <span style={{ display:'inline-block',width:'75%',color:'#0747A6',textAlign:'end'}} >90</span>
                      <span style={{ color:'#C4C4C4'}}>120</span>
                    </div>
                    <div style={{ display: "flex",justifyContent: "flex-end", }}>
                      <Button style={{ backgroundColor: "#0747A6", color: "#fff",}} onClick={()=>{
                        console.log( props)
                        // setMenu('ViewData')
                      }}>View Data</Button> 
                      <Button style={{ backgroundColor: "#F5F6F8", color: "#8993A4",}}>Advocate List</Button>
                    </div>
                  </div> 
                </Card.Content> 
              </Card>
          
            </Grid.Column>
            <Grid.Column width={3} > 
                <Card style={{ width:'100%',color:'#5E6C84'}}>
                    <Card.Content>
                        { getBigCard("Total Case",189,"+16",true)}
                        { getCard("Other",189,1)}
                        { getCard("Other",189,0)}
                        { getCard("Other",189,0)}
                    </Card.Content>
                  </Card>

              
            </Grid.Column>
            <Grid.Column width={3} > 
                { getBigCard("Total Case",189,"+16")}
                { getCard("Other",189,1)}
                { getCard("Other",189,0)}
                { getCard("Other",189,0)}
            </Grid.Column>
            <Grid.Column width={3} > 
                { getBigCard("Total Case",189,"+16")}
                { getCard("Other",189,1)}
                { getCard("Other",189,0)}
                { getCard("Other",189,0)}
            </Grid.Column>
         
        </Grid.Row>
        <Grid.Row>
            <Grid.Column width={8} >   
                <Card style={{ width:'100%' }}>
                    <Card.Content>
                      Map
                    </Card.Content>
                  </Card>
            </Grid.Column>
            
            <Grid.Column width={8} > 
                <Card style={{ width:'100%' }}>
                    <Card.Content>
                        <Line/> 
                    </Card.Content>
                  </Card> 
            </Grid.Column>
        </Grid.Row>
    </Grid> 

      
 
 
    </div>
  );
};

