import React, { useState } from "react"; 
import { Header,Dropdown,Modal,Button,Segment,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";
import { TitleArea ,Between,End,Empty,Badge,getBigCard,getCard} from '../pageCore/PageCore';

import Pine from '../pageCore/pine';
import Line from '../pageCore/line';
import Bar from '../pageCore/bar';
import BarBG from '../pageCore/barBg';

export default  () => { 
  const [open, setOpen] = useState(false);

  const [initProfile, setProfileCard] = useState(false);   
  const [initNotice, setNoticeCard] = useState(false);   
  const [initEmail, setEmailCard] = useState(false);   
  const [initPassword, setPasswordCard] = useState(false);   
  const [showModal, setShowModal] = useState(false);   
  const countryOptions = [
  { key: 'af', value: 'af', text: 'Afghanistan' },
  { key: 'ax', value: 'ax', text: 'Aland Islands' }, 
]

const options = [
  { key: 'angular', text: 'Angular', value: 'angular' },
  { key: 'css', text: 'CSS', value: 'css' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' }, 
] 
  return (
    <div > 
      <TitleArea title="Visualiztion"/> 
       

    <Grid>
        <Grid.Row> 
            
            <Grid.Column width={3} > 
                <Card style={{ width:'100%',color:'#5E6C84'}}>
                    <Card.Content>
                        { getBigCard("Total Case",189,"+16",true)}
                        { getCard("Other",189,1)}
                        { getCard("Other",189,0)}
                        { getCard("Other",189,0)}
                    </Card.Content>
                  </Card>

              
            </Grid.Column>
            <Grid.Column width={3} > 
                { getBigCard("Total Case",189,"+16")}
                { getCard("Other",189,1)}
                { getCard("Other",189,0)}
                { getCard("Other",189,0)}
            </Grid.Column>
            <Grid.Column width={3} > 
                { getBigCard("Total Case",189,"+16")}
                { getCard("Other",189,1)}
                { getCard("Other",189,0)}
                { getCard("Other",189,0)}
            </Grid.Column>
            <Grid.Column width={6} >   
              <Card >
                  <Card.Content>
                   <Pine/>
                  </Card.Content>
                </Card> 
          </Grid.Column>
         
        </Grid.Row>
        <Grid.Row>
            <Grid.Column width={8} >   
                <Card style={{ width:'100%' }}>
                    <Card.Content>
                      <Bar/> 
                    </Card.Content>
                  </Card>
            </Grid.Column>
            
            <Grid.Column width={8} > 
                <Card style={{ width:'100%' }}>
                    <Card.Content>
                      <BarBG/> 
                    </Card.Content>
                  </Card> 
            </Grid.Column>
        </Grid.Row>
    </Grid> 

      
 
 
    </div>
  );
};

