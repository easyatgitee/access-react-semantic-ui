import React, { useState } from "react"; 
import { Modal,Button,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";
import { TitleArea ,Between,End} from '../pageCore/PageCore';
export default  () => { 
  const [open, setOpen] = useState(false);
  const [mode, setMode] = useState(true); // 是否是主模式

  const [initProfile, setProfileCard] = useState(false);   
  const [initNotice, setNoticeCard] = useState(false);   
  const [initEmail, setEmailCard] = useState(false);   
  const [initPassword, setPasswordCard] = useState(false);   
  const [showModal, setShowModal] = useState(false);   
  const countryOptions = [
  { key: 'af', value: 'af', text: 'Afghanistan' },
  { key: 'ax', value: 'ax', text: 'Aland Islands' }, 
]
  return (
    <div> 
  
      <TitleArea title="View Data"/>
      <div style={{   display: mode ? "none" : "block", }}>
             <div>
                <span style={{ color: "#42526E",   fontSize:24 }}>  Migrant Workers Centre   </span><span style={{ backgroundColor: '#B3D4FF', color: '#0747A6', padding: '3px 10px',  borderRadius: 4, }}>June,2021</span>
                     
             </div>
             <div style={{ 
                 marginTop:10,
                backgroundColor: "#DFDFDF", color: "#000", width:'90%',height:'60vh',  display:'flex', justifyContent: "center",
             alignItems: "center",fontSize:30 }}>
                   Google Form embedded  
             </div>
             <div style={{ display: "flex",justifyContent: "flex-end", }}>
                <Button   onClick={()=>{   setMode(true); }} style={{ backgroundColor: "#fff", color: "#3F7DD9",}}>Back</Button>
                <Button style={{ backgroundColor: "#0747A6", color: "#fff",}}>  Export to CSV</Button> 
                
            </div>
       </div> 
      <div style={{   display: mode ? "block" : "none", }}>
        <Grid>
            <Grid.Row>
                <Grid.Column width={9} >    <Input fluid icon='search'  iconPosition='left' placeholder='Search...'/> </Grid.Column>
                <Grid.Column style={{ width:'12%'}}> 
                    <Select compact  style={{ width:180,  borderWidth: 0,  backgroundColor: "#F4F5F7", color: "#A0A9B7",}} placeholder='City ' options={countryOptions} /> 
                </Grid.Column>
                <Grid.Column style={{ width:'12%'}} > 
                    <Select compact style={{  width:180, borderWidth: 0,  backgroundColor: "#F4F5F7", color: "#A0A9B7",}} placeholder='Court Type ' options={countryOptions} /> 
                </Grid.Column>
                <Grid.Column style={{ width:'12%'}} > 
                    <Select compact  style={{ width:180,  borderWidth: 0,  backgroundColor: "#F4F5F7", color: "#A0A9B7",}} placeholder='Date' options={countryOptions} /> 
                </Grid.Column>
            </Grid.Row>
        </Grid> 
 
        <Table padded>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell> <Checkbox label={{ children: 'Select all' }} />   </Table.HeaderCell>
                <Table.HeaderCell>City</Table.HeaderCell>
                <Table.HeaderCell>Submission Date </Table.HeaderCell>
                <Table.HeaderCell>Court Type</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
                <Table.HeaderCell> 
                  <Button  style={{ backgroundColor: "#0747A6", color: "#fff",}}>
                    Export to CSV
                  </Button>
                      </Table.HeaderCell>
                <Table.HeaderCell>  </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
        
            <Table.Body>
              {
                  [1,2,3,4,5,6,7,8].map(e=>{
                      return <Table.Row>
                        <Table.Cell>  <Checkbox label={{ children: 'Make my profile visible' }} /> </Table.Cell>
                        <Table.Cell>John</Table.Cell>
                        <Table.Cell>2020-11-11</Table.Cell>
                        <Table.Cell>Family Law</Table.Cell>
                        <Table.Cell>NEW </Table.Cell>
                        <Table.Cell style={{   color: "#3F7DD8",}}>  Export to CSV   </Table.Cell>
                        <Table.Cell> <Button  onClick={()=>{   setMode(false); }} style={{ backgroundColor: "#F5F6F8", color: "#8993A4",}}> View </Button> </Table.Cell>
                      </Table.Row>
                  })
              } 
            </Table.Body>
            <Table.Footer>
              <Table.Row>
                <Table.HeaderCell colSpan='2'> </Table.HeaderCell>
                <Table.HeaderCell colSpan='4'>
                  <Pagination 
                  firstItem={null}
                  lastItem={null}
                  defaultActivePage={5} totalPages={10}  onPageChange={()=>{} }/>
                </Table.HeaderCell>
                <Table.HeaderCell colSpan='2'> </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>

      </div> 
 

    <Modal open={open} size="small" style={ {  width: 400 }}
      content={
        <div style={ {  padding:20, }} >
        <p style={ {  color:"#42526E",fontSize:18,fontWeight:600  }}>  Are you sure want to log out ?</p>
        <div style={{ display:'flex' ,  justifyContent: "flex-end",}}>
          <Button    onClick={() => {
            setOpen(false);
          }} size = "mini" style={{ backgroundColor: "#fff", color: "#8993A4", borderRadius:2,}}>Cancel</Button>
          <Button    onClick={() => {
            setOpen(false);
            
          }}   size = "mini" style={{ backgroundColor: "#0052CC", color: "#fff", borderRadius: 2,}}>Log Out</Button>
        </div> 
        </div>
      }
    />
    </div>
  );
};

