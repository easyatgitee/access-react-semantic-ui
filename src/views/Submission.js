import React, { useState,useEffect } from "react"; 
import { Dropdown,Modal,Button,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";
import { TitleArea ,Between,End} from '../pageCore/PageCore';
export default  () => { 
  const [open, setOpen] = useState(false);

  const [initProfile, setProfileCard] = useState(false);   
  const [initNotice, setNoticeCard] = useState(false);   
  const [initEmail, setEmailCard] = useState(false);   
  const [initPassword, setPasswordCard] = useState(false);   
  const [showModal, setShowModal] = useState(false);   
   
  const [select1, setSelect1] = useState(null);   
  const [select2, setSelect2] = useState(null);   
  const [select3, setSelect3] = useState(null);   
  const [createBtn, setShowCreate] = useState(false);   
  const countryOptions = [
  { key: 'af', value: 'af', text: 'Afghanistan' },
  { key: 'ax', value: 'ax', text: 'Aland Islands' }, 
]

  const createStyle  = { backgroundColor: "#0052CC", color: "#fff",} 
  const initBtn = {  backgroundColor: "#F5F6F8", color: "#8993A4", } 

  useEffect(() => {
    if(select1!=null && select2!=null && select3!=null   ){
      setShowCreate(true)
    }
},[select1,select2,select3])

  return (
    <div> 
 
      <TitleArea title="New Submission"/> 
        <Grid>
            <Grid.Row> 
                <Grid.Column width={2} > 
                  <Select   onChange={(e,v)=>{ setSelect1(v.value) } } style={{   borderWidth: 0,  backgroundColor: "#F4F5F7", color: "#A0A9B7",}} placeholder='Court Type' options={countryOptions} /> 
              </Grid.Column>
                <Grid.Column width={2} > 
                    <Select onChange={(e,v)=>{ setSelect2(v.value) } } style={{   borderWidth: 0,  backgroundColor: "#F4F5F7", color: "#A0A9B7",}} placeholder='Program name ' options={countryOptions} /> 
                </Grid.Column>
                <Grid.Column width={2} > 
                    <Select onChange={(e,v)=>{ setSelect3(v.value) } } style={{   borderWidth: 0,  backgroundColor: "#F4F5F7", color: "#A0A9B7",}} placeholder='Month' options={countryOptions} /> 
                </Grid.Column>
                <Grid.Column width={2} > 
                  <Button  style={ createBtn ? createStyle : initBtn }>New Submission</Button>
              </Grid.Column>
            </Grid.Row>
        </Grid> 
        
        <div style={{ backgroundColor: "#F5F6F8",margin:'10px 0', width:'100%',height:'60vh', color: "#7A8599", borderRadius:8,}}> 
           
        </div>

        <Grid>
            <Grid.Row> 
                <Grid.Column width={14} > 
                
                </Grid.Column> 
                <Grid.Column width={2} > 
                  <Button    onClick={() => { setOpen(true); }} size = "mini" style={{ backgroundColor: "#fff", color: "#8993A4", borderRadius:2,}}>Cancel</Button>
                  <Button    onClick={() => { setOpen(false);  }}   size = "mini" style={{ backgroundColor: "#0052CC", color: "#fff", borderRadius: 2,}}>Submit</Button>
              </Grid.Column>
            </Grid.Row>
        </Grid> 
  


    <Modal open={open} size="small" style={ {  width: 400 }}
      content={
        <div style={ {  padding:20, }} >
        <p style={ {  color:"#42526E",fontSize:18,fontWeight:600  }}>  Are you sure want to give up this form ?</p>
        <div style={{ display:'flex' ,  justifyContent: "flex-end",}}>
          <Button    onClick={() => { setOpen(false); }} size = "mini" style={{ backgroundColor: "#F5F6F8", color: "#7A8599", borderRadius:2,}}>No</Button>
          <Button    onClick={() => { setOpen(false); }}   size = "mini" style={{ backgroundColor: "#FFAB00", color: "#7A8599", borderRadius: 2,}}> Yes</Button>
        </div> 
        </div>
      }
    />
    </div>
  );
};