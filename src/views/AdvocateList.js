import React, { useState } from "react"; 
import { Dropdown,Modal,Button,Card,Icon,Grid,Select,Table,Pagination,Checkbox,Menu,Input } from "semantic-ui-react";
import { TitleArea ,Between,End} from '../pageCore/PageCore';
export default  () => { 
  const [open, setOpen] = useState(false);

  const [initProfile, setProfileCard] = useState(false);   
  const [initNotice, setNoticeCard] = useState(false);   
  const [initEmail, setEmailCard] = useState(false);   
  const [initPassword, setPasswordCard] = useState(false);   
  const [showModal, setShowModal] = useState(false);   
  const [title, setTitle] = useState("Advocate List");   // 页面title 
  const [mode, setMode] = useState("adv");   // 模式页面   adv group view 
  const countryOptions = [
  { key: 'af', value: '5', text: '10' },
  { key: 'ax', value: '5', text: '5' }, 
]

const options = [
  { key: 'angular', text: 'Angular', value: 'angular' },
  { key: 'css', text: 'CSS', value: 'css' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' }, 
]


const ProgramNmae = [
  { key: 'CARES program', text: 'CARES program', value: 'CARES program' },
  { key: 'Class program', text: 'Class program', value: 'Class program' }, 
]
const CourtType = [
  { key: 'Family Type', text: 'Family Type', value: 'Family Type' },
  { key: 'Class Type', text: 'Class Type', value: 'Class Type' }, 
]
  return (
    <div> 
      <TitleArea title={ title } back = {
       mode ==='group' ? <Button size="mini" style={{  backgroundColor: "#F5F6F8", color: "#42526E", }} onClick={()=>{  setMode("adv") } }>Back</Button> :null
      }/>
    
       <div style={{  display: mode === "adv" ? "block" : "none", }}>
        <Grid> 
          <Grid.Row>
             
              <Grid.Column style={{ width:'10%'}} > 
               <div style={{ display: 'flex' }}>
                  
                <span style={{ display:'inline-block',marginTop:10,marginRight:5}}>Show</span>
                <Select compact style={{   
                  Width:100,
                  display:'inline-block', borderWidth: 1,  
                  backgroundColor: "#F4F5F7", color: "#000",}} 
                placeholder='10  ' options={countryOptions} />  
                <span style={{ display:'inline-block',marginTop:10}}>entries</span>
           
               </div>
              </Grid.Column>
              <Grid.Column width={3}  > 
                
              </Grid.Column> 
              <Grid.Column width={7}  > 
                   
              </Grid.Column> 
              <Grid.Column width={4} > 
                <div  style={{ display: 'flex',width:'100%' }} >   
                    <span style={{ display:'inline-block',marginTop:10,marginRight:5}}>Search </span> 
                    <Input fluid    iconPosition='left'   style={{ display: 'flex',width:'100%' }} /></div>  
              </Grid.Column>
              
          </Grid.Row>
      </Grid> 

      <Table basic='very'>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Organization </Table.HeaderCell>
            <Table.HeaderCell>City</Table.HeaderCell>
            <Table.HeaderCell>Advocate Name</Table.HeaderCell>
            <Table.HeaderCell>Program Name</Table.HeaderCell>
            <Table.HeaderCell>Court Type</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell> 
          </Table.Row>
        </Table.Header>
    
        <Table.Body>
          {
            [1,2,3,4,5,6,7,8].map(e=>{
                return <Table.Row>
                  <Table.Cell onClick = {()=>{ setMode("group"); setTitle("Neison CARES Society"); }}> Neison CARES Society. </Table.Cell>
                  <Table.Cell>John</Table.Cell>
                  <Table.Cell>2020-11-11</Table.Cell>
                  <Table.Cell>program name</Table.Cell>
                  <Table.Cell>Family Law </Table.Cell> 
                  <Table.Cell style={{  color:'#0052CC'}} onClick = {()=>{  setMode("group"); setTitle("Neison CARES Society");   }}>  View Data </Table.Cell>
                </Table.Row>
            })
        } 
          
        </Table.Body>
        <Table.Footer>
            <Table.Row>
              <Table.HeaderCell colSpan='2'>Showing page 1 of 51 entries </Table.HeaderCell>
              <Table.HeaderCell colSpan='4'>
                <Pagination firstItem={null} lastItem={null} defaultActivePage={5} totalPages={10}  onPageChange={()=>{} }/>
              </Table.HeaderCell>
              <Table.HeaderCell colSpan='2'> </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
      </Table>
       </div>

       <div style={{  display: mode === "group" ? "block" : "none", }}>
        <Grid> 
          <Grid.Row>
              <Grid.Column width={9} >    <Input fluid icon='search'  iconPosition='left' placeholder='Search...'/> </Grid.Column>
              <Grid.Column style={{ width:'12%'}} > 
                  <Select  compact style={{   borderWidth: 0,  backgroundColor: "#F4F5F7", color: "#A0A9B7",}} placeholder='Advocate Name ' options={countryOptions} /> 
              </Grid.Column>
              <Grid.Column style={{ width:'12%'}} > 
                  <Select  compact style={{   borderWidth: 0,  backgroundColor: "#F4F5F7", color: "#A0A9B7",}} placeholder='Program Nmae ' options={ProgramNmae} /> 
              </Grid.Column> 
              <Grid.Column style={{ width:'12%'}}  > 
                <Select  compact style={{   borderWidth: 0,  backgroundColor: "#F4F5F7", color: "#A0A9B7",}} placeholder='Court Type ' options={CourtType} /> 
            </Grid.Column> 
          </Grid.Row>
      </Grid> 

      <Table basic='very'>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Submission Date  <Icon name="arrow up"/> </Table.HeaderCell>
            <Table.HeaderCell>Advocate Name</Table.HeaderCell>
            <Table.HeaderCell>Program Name</Table.HeaderCell>
            <Table.HeaderCell>Court Type</Table.HeaderCell>
            <Table.HeaderCell>Status</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell> 
          </Table.Row>
        </Table.Header>
    
        <Table.Body>
          {
            [1,2,3,4,5,6,7,8].map(e=>{
                return <Table.Row>
                  <Table.Cell>2020-11-11</Table.Cell>
                  <Table.Cell>Amy Blaney</Table.Cell>
                  <Table.Cell>Program Name</Table.Cell>
                  <Table.Cell>Family Law</Table.Cell>
                  <Table.Cell>  <span style={{   backgroundColor: '#FFF0B3', color: '#172B4D', padding: '3px 10px',  borderRadius: 4, }}>NEW</span> </Table.Cell> 
                  <Table.Cell style={{ color:'#0052CC'}} onClick={()=>{ setMode("view")  }}>  View  </Table.Cell>
                </Table.Row>
            })
        } 
          
        </Table.Body>
        <Table.Footer>
            <Table.Row>
              <Table.HeaderCell colSpan='2'> </Table.HeaderCell>
              <Table.HeaderCell colSpan='4'>
                <Pagination firstItem={null} lastItem={null} defaultActivePage={5} totalPages={10}  onPageChange={()=>{} }/>
              </Table.HeaderCell>
              <Table.HeaderCell colSpan='2'> </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
      </Table>
       </div>


       <div style={{   display: mode==="view" ? "block" : "none", }}>
        <div>
           <span style={{ color: "#42526E",   fontSize:24 }}>  New CARES Socity   </span>
           <span style={{ backgroundColor: '#B3D4FF', color: '#0747A6', padding: '3px 10px',  borderRadius: 4, }}>June,2021</span>
                
        </div>
        <div style={{ 
            marginTop:10,
           backgroundColor: "#DFDFDF", color: "#000", width:'90%',height:'60vh',  display:'flex', justifyContent: "center",
        alignItems: "center",fontSize:30 }}>
              Google Form embedded  
        </div>
        <div style={{ display: "flex",justifyContent: "flex-end", }}>
           <Button   onClick={()=>{   setMode("group"); }} style={{ backgroundColor: "#fff", color: "#3F7DD9",}}>Back</Button>
           <Button style={{ backgroundColor: "#0747A6", color: "#fff",}}  onClick={()=>{   setOpen(true); }} >  Export to CSV</Button> 
           
       </div>
   </div> 

    <Modal open={open} size="small" style={ {  width: 400 }}
      content={
        <div style={ {  padding:20, }} >
        <p style={ {  color:"#42526E",fontSize:18,fontWeight:600  }}> Export this form to.CSV ?</p>
        <div style={{ display:'flex' ,  justifyContent: "flex-end",}}>
          <Button    onClick={() => { setOpen(false); }} size = "mini" style={{ backgroundColor: "#fff", color: "#8993A4", borderRadius:2,}}>Cancel</Button>
          <Button    onClick={() => { setOpen(false); }}   size = "mini" style={{ backgroundColor: "#0052CC", color: "#fff", borderRadius: 2,}}>Confirm</Button>
        </div> 
        </div>
      }
    />
    </div>
  );
};

