import "semantic-ui-css/semantic.min.css";
import AdvocateDBoard from "./dashboards/AdvocateDBoard";
import A2JDBoard from "./dashboards/A2JDBoard";
import LoginPage from "./login/LoginPage";
import CreatePage from "./login/CreatePage";
import SendEmail from "./login/SendEmail";

import { Switch, Route } from "react-router-dom";
import PrivateRoute from "./private-route";
import React, { useState } from "react";

function App() {
  const [role, setRole] = useState(localStorage.getItem("role"));

  return (
    <div className="App">
      <Switch>
        <PrivateRoute
          exact
          path="/advocate"
          component={() => <AdvocateDBoard role={role} setRole={setRole} />}
        />
        <PrivateRoute
          exact
          path="/a2j"
          component={() => <A2JDBoard role={role} setRole={setRole} />}
        />
        <Route path="/sendEmail" component={SendEmail} />
        <Route path="/create" component={CreatePage} /> 
        <Route path="/" component={LoginPage} />
      </Switch>
    </div>
  );
}

export default App;
